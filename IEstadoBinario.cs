using System;

namespace Laboratorio6
{
    public enum EstadoBinario
    {
        Ligado,
        Desligado
    };

    interface IEstadoBinario
    {
        EstadoBinario Estado { get; }

        void Desligar();
        void Ligar();
    }
}
